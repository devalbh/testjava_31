/*Write a Java program to break an integer into a sequence of individual digits.

Write a Java program that reads an integer between 0 and 1000 and adds all the digits in the integer.*/
/**
 * @author Deval Bhagat
 */
package assignment1;

import java.util.Scanner;

public class AdditionOfDigits {
	static public int addition(int val){
		int sum=0,number;
		while(val>0){
			number=val%10;
			sum=sum+number;
			val=val/10;
		}
		return sum;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int sum;
		try(Scanner sc=new Scanner(System.in);){
		System.out.println("Input an integer between 0 and 1000 :"); 
		int userValue=sc.nextInt(); // Taking value from user
		sum=addition(userValue);
		System.out.println("The sum of all digits in "+userValue+" is : "+sum);
		individualeDigits();
		}
	}
	static public void individualeDigits(){
        System.out.println("Enter the number : ");
		try(Scanner sc=new Scanner(System.in)){
		int choice=sc.nextInt(); // Taking the input from user
		String s=String.valueOf(choice);
		char[] a=s.toCharArray(); // Storing all the digits to character array
		for(char i:a){
			System.out.println(i); // Print the numbers
		}
	}
}
